\documentclass[xcolor={dvipsnames,svgnames}]{beamer}
\usetheme{Amsterdam}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{listings}
\usepackage{remreset}
\usepackage[backend=biber,doi=false,isbn=false,url=false]{biblatex}
\usepackage[babel]{csquotes}
\usepackage{graphicx}
\usepackage[caption=false]{subfig}
\usepackage{lmodern}
\usepackage{microtype}
\usepackage{stmaryrd}
\usepackage{colortbl}
\usepackage{upgreek}
\usepackage{makecell}
\usepackage{qtree}
\usepackage{bussproofs}
\usepackage{booktabs}
\usepackage{calc}
\usepackage{tabularx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{xkeyval}
\usepackage{todonotes}
\usepackage{tikz}
\usepackage{pgfplots}
\usepackage{pgfplotstable}
\usetikzlibrary{arrows,arrows.meta,calc,shapes,decorations.pathreplacing,trees,backgrounds}
\def\UrlBreaks{\do\/\do-}

\presetkeys{todonotes}{inline}{}
\setbeamercolor{description item}{fg=blue}

\newenvironment{proenv}{\only{\setbeamercolor{local structure}{fg=green}}}{}
\newenvironment{conenv}{\only{\setbeamercolor{local structure}{fg=red}}}{}

\newcommand{\ignore}[1]{}
\newcommand{\tabitem}{~~\llap{\textbullet}~~}
\newcommand{\inner}[2]{\langle{} #1, #2 \rangle{}}
\newcommand{\norm}[1]{\left\lVert#1\right\rVert}
\newcommand{\vecTwo}[2]{\left(\begin{array}{c} #1 \\ #2 \end{array} \right)}
\newcommand{\vecThree}[3]{\left(\begin{array}{c} #1 \\ #2 \\ #3 \end{array} \right)}

\title{Ant Experience}
\subtitle{Advanced Functional Programming}
\subject{}
\keywords{Ant Experience}
\author{Germano Gabbianelli, Giovanni Garufi, Petra Geels,\\ Resly Suidgeest, Fabian Thorand}
\institute{Universiteit Utrecht}
\date{28-10-2015}

\definecolor{darkgreen}{rgb}{0,0.501960784,0}
\definecolor{darkred}{rgb}{0.639215686,0.082352941,0.082352941}
\definecolor{turquoise}{rgb}{0.168627451,0.568627451,0.68627451}
\lstset{
	language=Haskell,
    basicstyle=\small\ttfamily,
    breaklines=true,             % Automatische Zeilenumbrüche
    showstringspaces=false,      % Markierung von Leerzeichen in Strings (sieht ausgeschaltet einfach besser aus)
    tabsize=4,                   % Anzahl an Leerzeichen pro Tab
    keywordstyle=\color{blue},   % Stil der Schlüsselwörter (LaTeX Stil)
    commentstyle=\color{darkgreen},  % Stil der Kommentare (LaTeX Stil)
    stringstyle=\color{darkred},     % Stil von Strings (LaTeX Stil)
    % Mit emph legen wir eine Reihe an Keywords fest, denen wir dann mit emphstyle einen Stil zuweisen.
    emphstyle=\color{blue}
    }

% ----------- Befehle für Section-Page ----------------------------- %

\begin{document}

%----------- Titlefolie ---------------------------------------------%
\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}

\begin{frame}{Overview}
  \begin{enumerate}
    \item Ants as an EDSL
    \item Code Example
    \item Template Haskell to the Rescue
    \item Ant Code as a Monad
    \item Optimizing Ant Code
  \end{enumerate}
\end{frame}

\begin{frame}{Ants as an EDSL}
  Why an embedded language? \\
  Reuse Haskell as a "Macro System"
  
  \begin{itemize}
    \item (Higher-Order) Functions
    \item Monads (and related combinators)
    \item Module System (Code Reuse)
    \item etc.
  \end{itemize}
  
\end{frame}

\begin{frame}[fragile]{What it looks like}
  \begin{lstlisting}
  zigZagHome :: Ant ()
  zigZagHome = mdo
    ...
    zig <- labelled $ [ant|
      repeat {
        if flip 8 {
          turn left;
          goto zag;
        } else {
          try move else $evade(zig;;);
          $lookForWayHome;
        }
      }
      |]
    ...
  \end{lstlisting}
\end{frame}


\begin{frame}[fragile]{Parser}
  \begin{itemize}
    \item Parsec  with applicative style combinators
    \item Parses the high level language to AntCode AST
  \end{itemize}
  
  \begin{lstlisting}
type AntCode = [AntStmt]

parseAntCode 
  :: ParsecT String () Identity AntCode
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Embedding the High Level Language}
  We use Template Haskell
  \begin{itemize}
    \item Compiles high level AST to our EDSL (as Haskell Expression)
    \item Allows calling Haskell from inside the language
  \end{itemize}
  
  \begin{lstlisting}
ant :: QuasiQuoter
ant = QuasiQuoter { quoteExp  = compile, ... }

compile :: String -> ExpQ
compile src = case (parse parseAntCode "<input>" src) of
  Right code -> cins (Block code)
  Left error -> fail $ show error
  \end{lstlisting}
  
\end{frame}

\begin{frame}[fragile]{Template Haskell Code Generation}
  
  \begin{lstlisting}
cins :: AntStmt -> ExpQ
...
cins (Drop)       = [| dropFood |]
...
cins (Block code) = 
  [| mfix (\ ~ $args -> $block) >> return () |]
  where 
    labels = extractLabels code
    args   = tupP $ map (varP . mkName) labels
    block  = ccode (map (varE . mkName . suffix) 
                        labels) code
  \end{lstlisting}
  
\end{frame}

\begin{frame}[fragile]{Desugared High Level Language}
  \begin{lstlisting}
  zigZagHome :: Ant ()
  zigZagHome = mdo
    ...
    zig <- labelled $ loop $ \br cont ->
      if' (random 8)
        (do turn IsLeft
            goto zag )
        (do move `failWith` evade zig
            lookForWayHome )
    ...
  \end{lstlisting}
\end{frame}


\begin{frame}[fragile]{Code Generation as a Monad}
  We defined a monad transformer
  \begin{lstlisting}
newtype AntT m a = AntT { runAntT' 
  :: Program -> AntState 
  -> m (a, Program, AntState) }
  \end{lstlisting}
  and combinators to construct ant programs:
  \begin{lstlisting}
emit     :: ... => Instruction -> AntT m ()

labelled :: ... => AntT m a -> AntT m Label

if'      :: ... => BoolExpr -> AntT m () 
                -> AntT m () -> AntT m ()
  \end{lstlisting}
  and more.
\end{frame}

\begin{frame}[fragile]{Optimizer}
  \begin{itemize}
    \item Removes Duplicate States
    \item Removes Unreachable States
    \item Performs a static analysis
      \begin{lstlisting}
      if' (sense Ahead Rock)
        (if' move (...) (turn IsRight))
        (...)
      \end{lstlisting}
      $\leadsto$
      \begin{lstlisting}
      if' (sense Ahead Rock)
        (turn IsRight)
        (...)
      \end{lstlisting}
  \end{itemize}
\end{frame}


%\begin{frame}[shrink=10]{Literatur}
%  \nocite{Bishop2006,CourseeraAndrewNg,Russell02artificialintelligence}
%  \printbibliography[heading=none]
%\end{frame}

\end{document}