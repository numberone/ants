{-# LANGUAGE TemplateHaskell, QuasiQuotes, RecursiveDo, RecordWildCards #-}
{- | The Chosen Ant is our strategy we submitted for competition. It is
unfortunately not good.

Basic strategy:
  - on startup, the ants create a large funnel of markers around the ant hill,
    which point towards the ant hill by using two different markers on adjacent
    cells.

  - they walk in a random zig-zag walk until they find food

  - when they pickup food, they walk in a random zag-zig walk until they find
    the funnel or the home.

  - they follow the markers into the entrance of the food drop and exit
    by replacing the ant in the center of the food drop, which then becomes an
    explorer.

  - if an ant finds the enemy ant hill, it starts circling around it (but not
    on it, so if it gets killed, the enemy ants at least have to pickup the food
    and don't get it for free).

-}
module Main where

import Ants.Main
import Ants.CodeGen
import Ants.TH
import Ants.HighLevel.Choice
import Ants.HighLevel.Movement

import Control.Lens
import Control.Monad.Reader

-- | Wrapper around for the entry state of a food carrier.
data FoodCarrier
  = FoodCarrier
    { bringHome :: Label
    -- ^ label that should be jumped to if an ant acquires food.
    }

-- | Wrapper around the important states of a food carrier.
data ExplorerAnts
  = ExplorerAnts
    { startUpOuter :: Label
    -- ^ entry point for the ants on the outer layers
    , startUpInner :: Label
    -- ^ entry point the the ants on the innermost layer
    , zigZag :: Label
    -- ^ enters zig-zag exploring state
    }

-- | The ant sitting in the back of the food drop.
data CenterGuy
  = CenterGuy
    { becomeCenterGuy :: Label
    -- ^ should be jump to if an ant has assumed the position of the old center
    -- guy after dropping food
    }

-- * Global sensible names for markers, to avoid confusion

funnelInner, funnelOuter :: MarkerNumber
funnelInner = 0
funnelOuter = 1

foodDropTop, foodDropBottom, foodDropMarker :: MarkerNumber
foodDropTop = 2
foodDropBottom = 4
foodDropMarker = 3

funnelFoodFar, funnelFoodNear :: MarkerNumber
funnelFoodFar = 5
funnelFoodNear = 3

startupMarker :: MarkerNumber
startupMarker = 5

-- * Some strategy templates that are used in the larger ones

-- | Stop doing stuff (infinite loop that is never exited).
idle :: Ant ()
idle = loop $ \_ _ -> ignore (sense Ahead Food)

-- | Wait for n rounds.
wait :: Int -> Ant ()
wait n = replicateM_ n $ ignore $ sense Ahead Food

-- | Escape from a critical situation.
escape :: LeftOrRight -> Ant ()
escape dir = doWhile (Not move) (\_ _ -> turn dir)

-- | Evade an obstacle
evade :: Label -> Ant ()
evade next = [ant|
  if sense foe ahead {
    turn left; turn left; turn left;
  } else if sense rock leftAhead {
    $escape(IsLeft;;);
  } else {
    $escape(IsRight;;);
  }
  goto next;
  |]

-- | Tries to move forward, if that doesn't succeed because there's a rock
-- in front of us, we turn around. Otherwise, we try to make room for the ant
-- that is walking in the opposite direction of where we want to go.
moveOrEvade :: Ant ()
moveOrEvade = [ant|
  while ! move {
    if sense rock ahead {
      $turnAround;
    } else {
      turn left;
      if move {
        $turnAround;
        $forceMove;
        turn left; turn left;
      } else {
        turn right; turn right;
        if move {
          $turnAround;
          $forceMove;
          turn right; turn right;
        } else {
          turn left;
        }
      }
    }
  }
|]

-- | Follows the marker along the top of the ant hill to the food drop zone.
-- It takes a command to be executed while in the "kill zone" of our food drop,
-- where a lot of enemy ants get stuck.
followAntHillMarkerTop :: Ant () -> Ant ()
followAntHillMarkerTop waitInKillZone = [ant|
  repeat {
    if sense marker 2 ahead {
      $moveOrEvade;
    } else if sense marker 2 leftAhead {
      $waitInKillZone;
      turn left;
      $moveOrEvade;
    } else if sense marker 2 rightAhead {
      turn right;
      $moveOrEvade;
    } else {
      goto break;
    }
  }
  |]


-- | Follows the marker along the bottom of the ant hill to the food drop zone.
-- It takes a command to be executed while in the "kill zone" of our food drop,
-- where a lot of enemy ants get stuck.
followAntHillMarkerBottom :: Ant () -> Ant ()
followAntHillMarkerBottom waitInKillZone = [ant|
  repeat {
    if sense marker 4 ahead {
      $moveOrEvade;
    } else if sense marker 4 leftAhead {
      turn left;
      $moveOrEvade;
    } else if sense marker 4 rightAhead {
      $waitInKillZone;
      turn right;
      $moveOrEvade;
    } else {
      goto break;
    }
  }
  |]

-- | Find home in a zag-zig fashion. (As soon as the funnel around the home is
-- found, we hopefully find home).
foodCarrier :: CenterGuy -> Ant FoodCarrier
foodCarrier ~CenterGuy{..} = mdo
  startup <- labelled [ant|
  $turnAround;
  repeat 5 { try move else {} }
  mark 3;
  $moveOrEvade;
  mark 5;
  |]
  -- | looks for the funnel pointing home.
  let lookForWayHome = withCont $ \cont -> [ant|
    if sense marker 1 ahead {
      goto foundFunnel;
    } else if sense marker 1 leftAhead {
      turn left;
      goto foundFunnel;
    } else if sense marker 1 rightAhead {
      turn right;
      goto foundFunnel;
    }
    |]
  -- zig zag random walk
  zig <- labelled $
    [ant|
    repeat {
      if flip 8 {
        turn left;
        goto zag;
      } else {
        try move else $evade(zig;;);
        $lookForWayHome;
      }
    }
    |]
  zag <- labelled $
    [ant|
    repeat {
      if flip 8 {
        turn right;
        goto zig;
      } else {
        try move else $evade(zag;;);
        $lookForWayHome;
      }
    }
    |]
  -- follow funnel until food drop
  foundFunnel <- labelled [ant|
    $moveOrEvade;
    repeat 6 {
      if sense marker 0 ahead {
        while ! (sense marker 2 ahead || sense marker 4 ahead) {
          $moveOrEvade;
          $lookForWayHome;
        }
        $moveOrEvade;
        if sense marker 2 here {
          while ! (sense home rightAhead || sense marker 3 rightAhead) { turn left; }
          goto followTopMarker;
        } else {
          while ! (sense home leftAhead || sense marker 3 leftAhead) { turn right; }
          goto followBottomMarker;
        }
      } else {
        turn left;
      }
    }
    |]
  followTopMarker <- labelled [ant|
    $followAntHillMarkerTop(wait 80;;);
    while ! (sense marker 3 ahead && ! sense friend ahead) { turn right; }
    $forceMove;
    turn right;
    while ! sense home here { $forceMove; }
    drop;
    goto becomeCenterGuy;
    |]
  followBottomMarker <- labelled [ant|
    $followAntHillMarkerBottom(wait 80;;);
    while ! (sense marker 3 ahead && ! sense friend ahead) { turn left; }
    $forceMove;
    turn left;
    while ! sense home here { $forceMove; }
    drop;
    goto becomeCenterGuy;
    |]
  return FoodCarrier { bringHome = startup }

-- | Ants searching for food.
explorerAnts :: FoodCarrier -> Ant ExplorerAnts
explorerAnts ~FoodCarrier{..} = mdo
  -- startup code ran in the beginning
  startupOuter <- labelled $ mark startupMarker
  startupInner <- labelled $ do
    while (sense Here Home) (\_ _ -> ignore move)
    goto makeFunnel
  -- | Create a funnel around our ant hill
  makeFunnel <- labelled [ant|
    repeat 3 {
      repeat 4 {
        if sense marker 0 here || sense marker 1 here || ! move {
          goto zig;
        }
      }
      if sense marker 0 leftAhead || sense marker 0 rightAhead {
        repeat 2 { if move {} }
      }
      mark 0;
      if move {
        mark 1;
        if move {}
      } else {
        unmark 0;
      }
    }
    |]
  -- zig-zag food search
  -- this is inlined in zig and zag to evaluate the current perimeter
  let evaluateSituation = [ant|
    if sense foeHome ahead {
      goto circleFoeHome;
    } else if sense food ahead {
      $moveOrEvade;
      if pickUp {
        goto bringHome;
      } else {
      }
    } else if sense marker 3 ahead {
      $turnAround;
    }
    |]
  zig <- labelled [ant|
    repeat {
      if flip 4 {
        turn left;
        goto zag;
      } else {
        $evaluateSituation;
        try move else $evade(zig;;);
      }
    } |]
  zag <- labelled [ant|
    repeat {
      if flip 4 {
        turn right;
        goto zig;
      } else {
        $evaluateSituation;
        try move else $evade(zag;;);
      }
    } |]
  circleFoeHome <- labelled [ant|
    while ! ( sense foeHome leftAhead && ! sense foeHome ahead) {
      turn left;
    }
    repeat {
      while sense foeHome leftAhead {
        $forceMove;
      }
      turn left;
    }
    |]
  return ExplorerAnts
    { startUpOuter = startupOuter
    , startUpInner = startupInner
    , zigZag = zig
    }

-- | The remaining group of Ants at startup that is tasked with building the
-- food drop.
foodDropAnts :: Label -> Ant Label
foodDropAnts becomeExplorer = mdo
  -- go to the east end of the ant hill
  [ant|
  while (sense home ahead && ! sense marker 3 ahead) {
    $forceMove;
  }
  |]
  -- signalize that we are there, then distinguish inidividual ants by their position
  [ant|
  mark 3;
  if(! sense home ahead && ! sense home leftAhead && ! sense home rightAhead) {
    turn left;
    $forceMove;
    goto markFoodDropTop;
  } else {
    $turnAround;
    if sense friend ahead {
      $turnAround;
      if sense friend leftAhead && sense friend rightAhead {
        $forceMove;
        turn right;
        $forceMove;
        goto markFoodDropBottom;
      } else {
        goto moveRowsForward;
      }
    } else {
      $turnAround;
      if sense friend leftAhead && sense friend rightAhead {
        $wait(20;;);
        repeat 3 { $forceMove; mark 3; }
        $turnAround;
        repeat 3 { $forceMove; }
        $turnAround;
        goto foodDropCenter;
      } else {
        goto moveRowsForward;
      }
    }
  }
  $idle;
  |]
  -- code only run by the ant marking the top food drop queue
  markFoodDropTop <- labelled $ [ant|
    repeat 3 {
      turn left;
      while sense home leftAhead {
        mark 2;
        try move else {
          if sense friend ahead && sense marker 4 ahead {
            goto break;
          }
        }
      }
    }
    $turnAround;
    $followAntHillMarkerTop(wait 0;;);
    turn left;
    $moveOrEvade;
    mark 2;
    $moveOrEvade;
    mark 2;
    turn right;
    $moveOrEvade;
    mark 2;
    repeat 2 { turn right; }
    $moveOrEvade;
    mark 3;
    $idle;
    |]
  -- same for the bottom queue
  markFoodDropBottom <- labelled [ant|
    repeat 3 {
      turn right;
      while sense home rightAhead {
        mark 4;
        try move else {
          if sense friend ahead && sense marker 2 ahead {
            goto break;
          }
        }
      }
    }
    $turnAround;
    $followAntHillMarkerBottom(wait 0;;);
    turn right;
    $moveOrEvade;
    mark 4;
    $moveOrEvade;
    mark 4;
    turn left;
    $moveOrEvade;
    mark 4;
    repeat 2 { turn left; }
    $moveOrEvade;
    mark 3;
    $idle;
    |]
  -- moving the top and bottom rows forward
  moveRowsForward <- labelled $ do
    wait 20
    forceMove
    mark 3
    mapM_ unmark [foodDropTop, foodDropBottom]
    idle
  -- the ant sitting in the back of the food drop
  -- it oscillates between the food drop and the cell behind it to
  -- kill any enemy ant entering our food drop tunnel.
  -- if one of our ants enters the food drop, it leaves through the back exit
  -- and the ant that dropped the food becomes the new foodDropCenter ant.
  foodDropCenter <- labelled $ [ant|
    repeat {
      if sense friend ahead {
        $turnAround;
        goto becomeExplorer;
      } else {
        if move {
          $turnAround;
          $forceMove;
          $turnAround;
        }
      }
    }
    |]
  return foodDropCenter

-- | Predicate checking whether the current ant stands on the (now) outermost
-- layer during startup, while at the same time turning towards the boundary of
-- the ant hill.
isOutside :: BoolExpr -> BoolExpr
isOutside outsideCond = BoolAction $ \yes no ->
  withCont $ \over -> do
    replicateM_ 6 $
      if' outsideCond
        (yes `continueAt` over)
        (turn IsLeft)
    no

-- | Splits multiple layers from the initial ant configuration.
splitLayers :: [(BoolExpr, Ant ())] -> Ant () -> Ant ()
splitLayers [] def = def
splitLayers ((cnd, act):xs) def = splitLayer cnd act (splitLayers xs def)

-- | Splits a layer of ants from the initial configuration
splitLayer :: BoolExpr -> Ant () -> Ant () -> Ant ()
splitLayer outsideCond = if' (isOutside outsideCond)

main :: IO ()
main = defaultMain $ mdo
  -- our startup strategy
  splitLayers [ (Not (sense Ahead Home), goto $ startUpOuter explorers)
              , (sense Ahead (Marker startupMarker), goto $ startUpOuter explorers)
              , (sense Ahead (Marker startupMarker), goto $ startUpInner explorers) ]
              (goto makeFoodDrop)
  -- ants that build the food drop
  makeFoodDrop <- here
  foodDropCenter <- foodDropAnts becomeExplorer
  -- explorer ants
  explorers <- explorerAnts carriers
  -- carrier ants
  carriers <- foodCarrier (CenterGuy becomeCenterGuy)
  -- ants in this state are on the inside of the ant hill and should now leave
  -- it to become explorers
  let zigZagExp = zigZag explorers
  -- This abomination creates two "arms" to the left and right of the food drop
  -- exit which facilitates killing enemy ants that accidentally or purposefully
  -- get stuck around our food drop.
  let buildArm goleft goright = [ant|
    $goleft;
    if move {
      $idle;
    } else {
      $goright;
      if move {
        $goleft;
        if move {
          $goleft;
          if move {
            $idle;
          } else {
            $goright;
            if move {
              $goleft;
              if move {
                $idle;
              }
            }
          }
        }
      }
    }
    |]
  becomeExplorer <- labelled [ant|
    if flip 2 {
      $buildArm(turn left;;;turn right;;;);
    } else {
      $buildArm(turn right;;;turn left;;;);
    }
    goto zigZagExp;
    |]
  -- how an ants become the center guy of our food drop station
  becomeCenterGuy <- labelled [ant|
    $forceMove;
    $turnAround;
    goto foodDropCenter;
    |]
  return ()
