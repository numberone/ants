---
title:  'Specification of the High Level Ant Language'
author:
- Germano Gabbianelli
- Giovanni Garufi
- Petra Geels
- Resly Suidgeest
- Fabian Thorand
date: 30 October 2015
tags: [ants, haskell, EDSL]
abstract: |
  This file contains a description of the high level ant language that can be
  embedded in monadic ant code generation expressions using Template Haskell.
---

# Notation

We use angle brackets to indicate mandatory placeholders in the code snippets
below, e.g. `<statement>` is a placeholder for an arbitrary statement.
Optional parts are denoted with square brackets `[...]`.

# Statements

A `<statement>` is one of the following:

  (1) A control structure:

          if <condition> <statement> [else <statement>]
          try <condition> else <statement>
          while <condition> <statement>
          repeat [n] <statement>

  (2) A simple action:

          mark <markerNumber>;
          unmark <markerNumber>;
          drop;
          turn left;
          turn right;

  (3) A reference to a Haskell value:

          $<haskellVar>;
          $<haskellFun>(<argument>;;[<argument>;;[...]]);

  (4) A label or jump:

          <labelId>: <statement>
          goto <labelId>;

  (5) A block of zero or more statements

          {
            [...]
          }

# Conditions

There are several conditions that can be checked in the above control structures.
All low-level instructions that have two continuation states have there condition
counterpart.

  (1) `sense <cond> <direction>` checks one of the ant's basic senses, where
      `<cond>` can have one of the following values

      - `friend`
      - `friend with food`
      - `marker <i>` where `<i>` is a value from 0 to 5.
      - `home`
      - `foe`
      - `foe with food`
      - `foe marker`
      - `foe home`
      - `food`
      - `rock`

      and `<direction>` can be one of the following

      - `here`
      - `ahead`
      - `left ahead`
      - `right ahead`

  (2) `pickup` returns `true` on success and `false` if there were no food

  (3) `move` returns `true` if the move succeeds and `false` if the target ahead is blocked

  (4) `flip <p>` returns `true` if the random number is 0 and `false` otherwise

  (5) A reference to a Haskell value defining the condition

          $<haskellVar>;
          $<haskellFun>(<argument>;;[<argument>;;[...]]);

  (6) Conditions can be combined using the operators `&&` (logical and)
      `||` (logical or) and `!` (logical not). The `&&` and `||` operators
      are evaluated left to right and are shortcutting. This is important to
      note in case conditions have side effects.

# Description of the Control Structures

An `if` statement evaluates a condition and chooses one of the two branches
accordingly.

For movement and picking up food, its more convenient to only have an else branch.
This is what the `try` statement is for. It evaluates the condition and only
executes the statement if the condition evaluates to false.

The `while` loop shows the expected behavior. It always evaluates the condition
before entering the loop body. If the condition evaluates to `false`, the
program flow continues after the loop without executing the body.

Then, there is `repeat` for executing a block a predefined number of times.
`repeat`-loops are unrolled at compile time, as the low level ant code is
lacking variables.
If the number of repetitions is omitted, an infinite loop is produced.

# Working with Haskell Values

The high level ant language is meant to be used by means of the `ant`
quasiquoter which translates it into a Haskell expression that is spliced
into the source code.

Therefore, all Haskell identifiers that are in scope at the insertion point
can be referenced in a condition or statement.

A reference to a Haskell identifier inside a condition requires a value
of type `BoolExpr`, a reference in a statement has to match the type of the
surrounding monadic computation.

An argument to a Haskell function call can be one of the following:

  (1) A statement passed as a function argument has type `forall m. MonadFix m => AntT m ()`.
      It is therefore automatically specialized if the function requires a specific monad.

  (2) A Haskell expression which is parsed using the `haskell-src-exts` package.
      Therefore, almost any Haskell expression can be used. Since parsing that
      is rather difficult, the Haskell expression must not contain two
      consecutive semicolons (`;;`). These are used to mark the end of an argument.

Label statements are transformed to Haskell identifiers of type `Label` and are
in scope inside a block and all nested blocks, but not on higher levels, due to
the nature of translation. They can be used anywhere inside the block where an
arbitrary Haskell expression of type `Label` can be inserted.

The `goto` expression expects a name of a `Label`. Since all labels are just
Haskell identifiers, the `goto` statement can reference any `Label` that is
in scope.
