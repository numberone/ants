{-# LANGUAGE TemplateHaskell, OverloadedStrings #-}
{-| This module provides the facilities for translating high level ant code
to Haskell expressions that can be used with the "Ant.CodeGen" module.

Ususally, only the 'ant' QuasiQuoter should be needed. For a usage example
see below.

-}
module Ants.TH
    (
    -- * QuasiQuoter
      ant
    -- * Compilation
    , compile
    , cins
    , ccond
    -- * Debugging
    , debug
    ) where

import Control.Monad.Fix
import Data.Data (Data)
import Data.Text (replace, pack, unpack)
import Language.Haskell.TH
import Language.Haskell.TH.Quote (dataToExpQ, QuasiQuoter (..))
import Language.Haskell.Meta.Parse (parseExp)

import Ants.CodeGen hiding (Instruction (..))
import Ants.Parser.Parser (parse, parseAntCode)
import Ants.Parser.AST (AntCode, AntCond (..), AntStmt (..), AntExpr (..), AntName)

-- | Utility function to convert a @Data a@ to ExpQ
dataQ :: Data a => a -> ExpQ
dataQ = dataToExpQ (const Nothing)

-- | The ant QuasiQuoter calls the 'compile' function to compile expressions.
-- It parses the code using the 'parseAntCode' parser and compiles all the
-- instructions if the parse was successful.
compile :: String -> ExpQ
compile src = case (parse parseAntCode "<input>" src) of
    Right code -> cins (Block code)
    Left error -> fail $ show error

-- | The high level ant language quasiquoter. It only needs to support expressions,
-- hence all fields except 'quoteExp' are 'undefined'. Usage example:
--
-- @
-- moveOrTurnLeft :: MonadFix m => AntT m ()
-- moveOrTurnLeft = [ant|
--   try move else {
--     turn left;
--   } |]
-- @
ant :: QuasiQuoter
ant = QuasiQuoter { quoteExp  = compile
                  , quotePat  = undefined
                  , quoteDec  = undefined
                  , quoteType = undefined
                  }

-- | Compiles an 'AntStmt' to a Haskell expression represented as 'ExpQ'.
-- Special attention must be taken when compiling a Block statement; since
-- TH does not support __mdo__ notation we have to manually call 'mfix'.
-- The labels are collected and passed as the first argument to ccode.
cins :: AntStmt -> ExpQ
cins (Mark mn)           = [| mark mn |]
cins (Unmark mn)         = [| unmark mn |]
cins (Turn lor)          = [| turn $(dataQ lor) |]
cins (Drop)              = [| dropFood |]
cins (Repeat Nothing s)  = [| loop (\break continue -> $(cins s)) |]
cins (Repeat (Just n) s) = [| replicateM_ n $(cins s) |]
cins (Try cond stmt)     = [| failWith $(ccond cond) $(cins stmt) |]
cins (Goto name)         = [| goto $(varE . mkName $ name) |]
cins (HaskellApp n xs)   = capp ((varE . mkName) n) xs
cins (While c s)         = [| while $(ccond c) (\break continue -> $(cins s)) |]
cins (If cond tS mE)     = [| if' $(ccond cond) $(cins tS) $(celse mE) |]
cins (Block code)        = [| mfix (\ ~ $args -> $block) >> return () |]
    where labels = extractLabels code
          args   = tupP $ map (varP . mkName) labels
          block  = ccode (map (varE . mkName . suffix) labels) code


-- | Specialized version of 'cins' to compile (Haskell) function application.
capp :: ExpQ -> [AntExpr] -> ExpQ
capp f [] = f
capp f ((AntExprStmt s):args)    = capp (appE f (cins s)) args
capp f ((AntExprHaskell s):args) = capp (appE f arg) args
    where arg = case parseExp s of
                    Right code -> return code
                    Left error -> fail error

-- | Specialized version of 'cins' to compile an else branch which may or
-- may not be present.
celse :: Maybe AntStmt -> ExpQ
celse mE = maybe [| return () |] (\stmt -> cins stmt) mE

-- | Compiles a list of AntStatment to an 'ExpQ' calling 'cins' on every instruction
-- and chaining the results using monadic bind.
-- The first argument is a list of labels (variable expressions) that were found
-- in the current block of code.
-- When the AntStmt list is empty the labels are returned as a tuple;
-- this is needed for the 'mfix' call when calling 'cins' on a block of code.
ccode :: [ExpQ] -> AntCode -> ExpQ
ccode r [] = [| return $(tupE r) |]
ccode r ((Label name stmt):code) =
    [| labelled $(cins stmt) >>= \ $arg -> $(ccode r code) |]
    where arg = (varP . mkName . suffix) name
ccode r (stmt:code) = [| $(cins stmt) >> $(ccode r code) |]


-- | Compile an 'AntCond' to a Haskell expression.
ccond :: AntCond -> ExpQ
ccond (CondSense c d     ) = [| sense $(dataQ d) $(dataQ c) |]
ccond (CondFlip x        ) = [| random x |]
ccond (CondPickUp        ) = [| pickUp |]
ccond (CondMove          ) = [| move |]
ccond (CondAnd x y       ) = [| $(ccond x) :&: $(ccond y) |]
ccond (CondOr  x y       ) = [| $(ccond x) :|: $(ccond y) |]
ccond (CondNot x         ) = [| Not $(ccond x) |]
ccond (CondHaskell n args) = capp ((varE . mkName) n) args

-- | Searches for all the top-level labels in a block of code and returns a list
-- of the found labels.
extractLabels :: AntCode -> [AntName]
extractLabels ((Label name _):code) = name:(extractLabels code)
extractLabels (_:code) = extractLabels code
extractLabels [] = []

-- | Adds a prefix and a suffix to the extracted labels to make them pseudo-unique.
-- We are not sure how name shadowing is handled in our case, so we decided
-- to give a different name to the labels used in the 'mfix' lambda compared
-- to the actual bindings (using @>>=@).
suffix :: AntName -> AntName
suffix name = "__LABEL__" ++ name ++ "__LABEL__"


-- | Utility function to pretty print the compiled AntCode
debug x = runQ (compile x) >>= \exp -> do
    let f = foldl (\str p -> replace p "" str) (pack $ pprint exp) [
                "Ants.CodeGen.", "GHC.Base.", "Control.Monad.Fix.",
                "GHC.Tuple."]
    putStrLn $ unpack f
