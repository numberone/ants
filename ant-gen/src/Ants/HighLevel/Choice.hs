{- | A module containing high-level functions for working with probabilities
in ant code.

It provides function for randomly choosing one of many continuation states which
generate the required 'Flip' instructions for that.
-}
module Ants.HighLevel.Choice
  ( uniformV
  , uniform
  , choose
  ) where

import Control.Arrow
import Data.Ratio
import Data.List
import Data.Bits

import qualified Data.Vector as V

import Ants.CodeGen

-- | Generates a uniform choice between all states in the vector.
-- In principle, it is based on exponentiation by squaring.
-- In each step, if the total number of choices is even, it needs one @Flip@
-- instruction to choose between the two halves, splitting them recursively.
-- If the total number of choices is odd, it generates one @Flip@ instruction
-- to choose between the first state and the rest, and then another one to
-- actually halve the number of choices.
--
-- This ensures, that the algorithm uses at most roughly @2 * log_2(n)@
-- @Flip@ instructions, thus it's @O(log(n))@ in generated code size and runtime
-- execution speed (where @n@ is the number of states).
uniformV :: MonadFix m => V.Vector (AntT m ()) -> AntT m ()
uniformV cs
  | size <= 0 = return ()
  | size == 1 = V.head cs
  | even size = random_ 2 (uniformV $ V.slice 0 half cs) (uniformV $ V.slice half half cs)
  | otherwise = random_ size (V.head cs) (uniformV $ V.tail cs)
  where
    size = V.length cs
    half = size `div` 2

-- | Generates a uniform choice between all states in the list.
-- See 'uniformV' for a detailed explanation.
uniform :: MonadFix m => [AntT m ()] -> AntT m ()
uniform = uniformV . V.fromList

-- | Takes a weighted list of states and generates a random choice for them.
--
-- Suppose the sum of all weights is @n@, then an individual state with weight
-- @w@ has probability @w/n@.
--
-- This function is implmeneted using 'uniform' by replicating each state
-- according to its weight.
--
-- This implies, that the number of generated states is bounded by
-- @2 * log_2(n)@, and hence is in @O(log(n))@ (as well as the runtime execution speed).
--
-- While this is good for many states with little variation in the weights,
-- it induces a significant overhead for corner cases. The following example
-- shows that in the worst case, we generate arbitrarily more @Flip@ instructions
-- than the optimal solution:
--
-- Suppose we have an input @[(1,x), (m,y)]@, then the optimal solution is
-- one @Flip@ instruction with parameter @1 + m@ while this function will generate
-- a number of instructions somewhere between @log_2(1+m)@ and @2 * log_2(1+m)@.
--
-- __Note__ that all base two logarithms in the above calculations probably
-- have to be rounded up to the next integer.
choose :: MonadFix m => [(Int, AntT m ())] -> AntT m ()
choose = uniform . concatMap (uncurry replicate)
