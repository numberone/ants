{-| This module contains some movement related functions that could be useful
when implementing ant strategies.
-}
module Ants.HighLevel.Movement where

import Ants.CodeGen

-- | Turns left for a specified number of times.
turnLeft :: MonadFix m => Int -> AntT m ()
turnLeft n = replicateM_ n (turn IsLeft)

-- | Turns right for a specified number of times.
turnRight :: MonadFix m => Int -> AntT m ()
turnRight n = replicateM_ n (turn IsRight)

-- | Turns around.
turnAround :: MonadFix m => AntT m ()
turnAround = replicateM_ 3 (turn IsLeft)

-- | Repeatedly tries moving forward until success.
forceMove :: MonadFix m => AntT m ()
forceMove = while (Not move) (\_ _ -> return ())