{-# LANGUAGE DeriveFoldable        #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveTraversable     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}
{- | This module provides several optimization strategies that can be used to
reduce the number of states in ant code.

The program transformations rely on a graph representation provided by the
"Ants.Util.Graph" module.

The following three optimizations have been implemented:

  (1) Removing unreachable code ('deleteUnreachableCode'), i.e. states that can
      never be reached.

  (2) Removing duplicate states ('deleteDuplicates' and 'deleteDuplicatesOnce'),
      i.e. states that behave exactly the same (same action and same continuation states).

  (3) Performing a static analysis on the ant code ('staticAnalysis')
      exploiting knowledge about certain senses at compile time.
      In the process, instructions that are unnecessary given the knowledge at
      that point are removed ('simplify').

-}
module Ants.Optimizer
  (
  -- * Generalized Instruction Type
    InstructionF (..)
  , generalized
  -- * Code Graph
  , CodeGraph
  , codeGraph
  , buildGraph
  , graphToProgram
  -- * Static analysis
  , Knowledge (..)
  , prettyPrintKnowledge
  , HasSideEffects (..)
  , combineKnowledge
  , knowledgeBy
  , staticAnalysis
  -- * Optimizations
  , simplify
  , deleteUnreachableCode
  , deleteDuplicates
  , deleteDuplicatesOnce
  , optimize
  ) where

import           Control.Lens
import           Control.Monad.State
import qualified Data.Foldable          as F
import           Data.Function
import           Data.List              (sortBy)
import qualified Data.IntMap            as IntMap
import           Data.Map               (Map)
import qualified Data.Map               as Map
import           Data.Maybe
import           Data.Ord
import qualified Data.Set               as Set
import           Text.Printf
import qualified Data.Vector            as V

import           Ants.CodeGen
import           Ants.Util.Graph        (Graph)
import qualified Ants.Util.Graph        as Graph

import           Simulator              (AntState, Condition (..),
                                         Instruction (..), LeftOrRight (..),
                                         MarkerNumber, SenseDir (..))
-- | This data type is the same as 'Instruction', but all occurrences of
-- 'AntState' have been extracted to a type argument. This allows to use
-- this data type as 'Functor', 'Foldable' and 'Traversable', providing functions
-- to map and traverse the follow-up states for free.
-- (By letting GHC derive these instances.)
--
-- It holds that @Instruction == InstructionF AntState@, as witnessed by the
-- isomorphism 'generalized'.
data InstructionF a
   = SenseF SenseDir a a Condition
   | MarkF MarkerNumber a
   | UnmarkF MarkerNumber a
   | PickUpF a a
   | DropF a
   | TurnF LeftOrRight a
   | MoveF a a
   | FlipF Int a a
   deriving (Show, Read, Eq, Ord, Functor, F.Foldable, Traversable)

-- | Type of the code graph.
type CodeGraph = Graph AntState InstructionF

-- | Isomorphism between the original instruction type and the more generalized
-- variant.
generalized :: Iso' Instruction (InstructionF AntState)
generalized = iso embed retract where
  embed instr = case instr of
    Sense d x y c -> SenseF d x y c
    Mark n x -> MarkF n x
    Unmark n x -> UnmarkF n x
    PickUp x y -> PickUpF x y
    Drop x -> DropF x
    Turn d x -> TurnF d x
    Move x y -> MoveF x y
    Flip p x y -> FlipF p x y
  retract instrf = case instrf of
    SenseF d x y c -> Sense d x y c
    MarkF n x -> Mark n x
    UnmarkF n x -> Unmark n x
    PickUpF x y -> PickUp x y
    DropF x -> Drop x
    TurnF d x -> Turn d x
    MoveF x y -> Move x y
    FlipF p x y -> Flip p x y

-- | An Isomorphism between the program representation and the code graph representation.
codeGraph :: Iso' Program CodeGraph
codeGraph = iso buildGraph graphToProgram

-- | Builds a an explicit graph structure from a program. This graph allows
-- reverse lookups of jumps (i.e. retrieving the set of states that jump to a
-- given state).
-- In the process, unreachable states are skipped.
buildGraph :: Program -> CodeGraph
buildGraph prog = Graph.unfoldGraph gen (prog ^. entryPoint) id where
  gen idx = case prog ^. instructions . at idx of
    Nothing -> error "invalid state number: does not exist"
    Just instr -> instr ^. generalized

-- | Converts a code graph back to a program.
graphToProgram :: CodeGraph -> Program
graphToProgram g = Program
  { _entryPoint = view Graph.initialNode g
  , _instructions =  IntMap.fromAscList $ Map.toAscList $
      views Graph.nodes (fmap $ view (from generalized)) g
  }

-- | Performs 'deleteDuplicatesOnce' until no more states can be removed.
-- Maybe 'deleteDuplicatesOnce' is enough, I am not sure whether it will
-- exhaustively remove all duplicates in one go.
deleteDuplicates :: CodeGraph -> CodeGraph
deleteDuplicates g = go (Graph.size g) g where
  go n oldG =
    let newG = deleteDuplicatesOnce oldG
        newN = Graph.size newG
    in if newN == n then newG else go newN newG


-- | A heuristic that searches for duplicate states in the code graph.
-- That means, that if two states contain the same instruction (including the
-- same jump targets), they get merged.
-- The algorithm does this by first sorting all nodes by in-degree, throwing
-- away those with in-degree less than two. Then the parents of these nodes
-- are candidates for merging.
-- Since two instructions must be exactly the same for merging, two mergable
-- states are always predecessors of the same node.
-- When two parents are merged, there parents are first considered for merging
-- before trying out the remaining candidates.
--
-- While this algorithm will detect and merge all mergable states that are
-- present in the original arguments, I am not sure whether it will transitively
-- perform all possible merges. Therefore, 'deleteDuplicates' performs this
-- pruning until no more states have been removed.
deleteDuplicatesOnce :: CodeGraph -> CodeGraph
deleteDuplicatesOnce g = execState (elim $ candidates nodesWithID) g where
  -- list of states paired with their in-degree
  nodesWithID = map withInDeg $ views Graph.nodes Map.keys g
  withInDeg i = (i, g ^. Graph.inDegree i)
  -- canditate starting points for elimniation (must have at least to parents)
  candidates = map fst . sortBy (comparing $ views _2 Down) . filter (views _2 (>1))

  -- tries to merge different parents of @node@ that do the same thing.
  elim [] = return ()
  elim (node:xs) = do
    currentGraph <- get
    -- traverse all predecessors of the node
    (_, modifiedSet) <- flip execStateT (Map.empty, Set.empty) $
      iforOf_ (Graph.predecessors node) currentGraph $ \predIdx predVal -> do
        there <- use (_1 . at predVal)
        case there of
          -- if the node only occurred once, remember it
          -- Note that the node value is used as key
          Nothing -> _1 . at predVal .= Just predIdx
          -- If a node with the same value already exists, merge it
          Just oldIdx -> do
            lift $ modify $ Graph.mergeNodes predIdx oldIdx
            _2 %= Set.insert oldIdx
    -- all nodes that have been merged are now candidates for merging their parents,
    -- because parents could have become identical by merging their children
    let newCandidates = candidates $ map withInDeg $ Set.elems modifiedSet
    elim (newCandidates ++ xs)

-- | Knowledge about stuff at the current position. As long as the ant does not
-- move, this shouldn't change.
data Knowledge = Knowledge
  { _sensesHere   :: Map Condition Bool
  -- ^ Stores the senses at the current position of the ant.
  , _sensesSurroundings :: V.Vector (Map Condition Bool)
  -- ^ Stores senses about all surroundings, not just the ones we can actually sense
  -- Ahead is 0, RightAhead is 1 and LeftAhead is 5.
  -- __Note__ that currently only persistant senses are stored outside the current
  -- cell (namely 'Rock', 'Home' and 'FoeHome')
  , _carryingFood :: Maybe Bool
  -- ^ Knowledge whether the ant is currently carrying food.
  }
  deriving (Eq, Ord, Show, Read)
makeLenses ''Knowledge

noKnowledge :: Knowledge
noKnowledge = Knowledge Map.empty (V.replicate 6 Map.empty) Nothing

-- | Converts a 'SenseDir' other than 'Here' to the corresponding index in
-- '_sensesSurroundings'.
senseDirToIndex :: SenseDir -> Int
senseDirToIndex Ahead = 0
senseDirToIndex LeftAhead = 5 -- == (-1) `mod` 6
senseDirToIndex RightAhead = 1
senseDirToIndex Here = error "not a surrounding position"

-- | Lens into the knowledge map of the surrounding cell given by the index argument.
sensesAround :: Int -> Lens' Knowledge (Map Condition Bool)
sensesAround dir = sensesSurroundings . singular (ix dir)

-- | Lens into the knowledge map of a cell addressable by 'SenseDir'.
senses :: SenseDir -> Lens' Knowledge (Map Condition Bool)
senses Here = sensesHere
senses other = sensesAround (senseDirToIndex other)

-- | Checks whether the information accessed by the getter is certainly
-- of the given value.
isCertainly :: Eq a => a -> Getter Knowledge (Maybe a) -> Knowledge -> Bool
isCertainly x whereIs kb = case kb ^. whereIs of
  Just y -> x == y
  _ -> False

-- | Checks whether the information refered to by the getter is uncertain.
isUncertain :: Getter Knowledge (Maybe a) -> Knowledge -> Bool
isUncertain whereIs = has (whereIs . _Nothing)

-- | Wraps a known value.
known :: a -> Maybe a
known = Just

-- | Represents an unknown value.
unknown :: Maybe a
unknown = Nothing

-- | @incMod m i@ increases @i@ by one modulo @m@.
incMod :: Int -> Int -> Int
incMod m x = (x + 1) `mod` m

-- | @incMod m i@ decreases @i@ by one modulo @m@.
decMod :: Int -> Int -> Int
decMod m x = (x - 1) `mod` m

-- | Combines two sets of knowledge.
-- Conflicting information is assumed to be uncertain and therefore removed.
combineKnowledge :: Knowledge -> Knowledge -> Knowledge
combineKnowledge k1 k2 = Knowledge
  { _sensesHere   = mergeInfo (k1 ^. sensesHere) (k2 ^. sensesHere)
  , _sensesSurroundings = V.zipWith mergeInfo (k1 ^. sensesSurroundings) (k2 ^. sensesSurroundings)
  , _carryingFood = (combineInfo `on` _carryingFood) k1 k2
  }
  where
    -- If information is only present in one of the maps, it is dropped.
    -- If information for a condition is present in both maps, it is only
    -- kept, if it is the same.
    mergeInfo = Map.mergeWithKey (\_ x y -> if x == y then Just x else Nothing) (const Map.empty) (const Map.empty)

    combineInfo (Just x) (Just y) | x == y = Just x
    combineInfo _ _ = Nothing

-- | Specialized boolean: either there are side effects or there are none.
data HasSideEffects = Pure -- ^ instruction is pure (side effect free)
                    | Impure -- ^ instruction has side effects
  deriving (Eq, Ord, Show, Read, Enum, Bounded)

-- | Returns for each continuation state whether it is reachable given the
-- current knowledge, and if it is, returns the knowledge acquired in that branch
-- and whether that branch has side-effects or not.
--
-- This is where the deduction rules for the static analysis are implemented.
knowledgeBy :: InstructionF AntState -> Knowledge -> InstructionF (Maybe (Knowledge, HasSideEffects, AntState))
knowledgeBy instr cur = case instr of
    s@(SenseF d st1 st2 c) ->
      case cur ^. senses d . at c of
        Just False
          | sensePersistant d c -> SenseF Here unreachable (reachable cur Pure st2) c
        Just True
          | sensePersistant d c -> SenseF Here (reachable cur Pure st1) unreachable c
        _
          | sensePersistant d c ->
            let ifTrue  = cur & senses d . at c .~ known True
                ifFalse = cur & senses d . at c .~ known False
            in SenseF Here (reachable ifTrue Pure st1) (reachable ifFalse Pure st2) c
          | otherwise -> fmap (reachable cur Pure) s
    MarkF n st
      | isCertainly True (sensesHere . at (Marker n)) cur
        -> MarkF n (reachable cur Pure st)
      | otherwise
        -> MarkF n $ reachable (cur & sensesHere . at (Marker n) .~ known True) Impure st
    UnmarkF n st
      | isCertainly False (sensesHere . at (Marker n)) cur
        -> UnmarkF n $ reachable cur Pure st
      | otherwise
        -> UnmarkF n $ reachable (cur & sensesHere . at (Marker n) .~ known False) Impure st
    PickUpF st1 st2
      | isCertainly True carryingFood cur
        || isCertainly False (sensesHere . at Food) cur
         -> PickUpF unreachable (reachable cur Pure st2)
      | isCertainly False carryingFood cur &&
        isCertainly True (sensesHere . at Food) cur ->
          let carrying = cur & carryingFood         .~ known True
                             & sensesHere . at Food .~ unknown
          in PickUpF (reachable carrying Impure st1) unreachable
      | otherwise ->
          let failReason = (cur ^. carryingFood) `mplus` (cur ^. sensesHere . at Food)
              failed   = cur & sensesHere . at Food .~ failReason
                             & carryingFood         .~ failReason
              carrying = cur & carryingFood         .~ known True
                             & sensesHere . at Food .~ unknown
          in PickUpF (reachable carrying Impure st1) (reachable failed Pure st2)
    DropF st
      -- if we are currently carrying food
      | isCertainly True carryingFood cur ->
          let dropped = cur & carryingFood         .~ known False
                            & sensesHere . at Food .~ known True
          in DropF $ reachable dropped Impure st
      -- if we are uncertain whether we are carrying food and there was
      -- no food at the drop site, we cannot be sure anymore.
      | isUncertain carryingFood cur ->
          let foodThere = isCertainly True (sensesHere . at Food) cur
              dropped = cur & carryingFood         .~ known False
                            & sensesHere . at Food .~ if foodThere then known True else unknown
          in DropF $ reachable dropped Impure st
      -- otherwise everything remains as it is
      | otherwise -> DropF $ reachable cur Pure st
    -- Turning does not change our information about the current cell,
    -- but moves the relative position of our surrounding cells
    TurnF dir st ->
      let dirOff = if dir == IsRight then 1 else (-1)
          new = flip execState cur $ forM_ [0..5] $ \i ->
                  sensesAround i .= view (sensesAround $ (i+dirOff) `mod` 6) cur
      in TurnF dir $ reachable new Impure st
    -- Moving requires us to replace information about the current cell with the next one
    MoveF st1 st2
      | isCertainly True (senses Ahead . at Rock) cur ->
        MoveF unreachable (reachable cur Pure st2)
      | otherwise ->
        let ifMoved = cur & sensesHere        .~ view (senses Ahead) cur
                          & senses Ahead      .~ Map.empty
                          & senses LeftAhead  .~ Map.empty
                          & senses RightAhead .~ Map.empty
                          & sensesAround 4    .~ view (sensesAround 5) cur
                          & sensesAround 2    .~ view (sensesAround 1) cur
                          & sensesAround 3    .~ views (senses Here) removeNonPersitant cur
        in MoveF (reachable ifMoved Impure st1) (reachable cur Pure st2)
    FlipF p st1 st2 -> FlipF p (reachable cur Pure st1) (reachable cur Pure st2)
  where
    reachable newKnowledge purity target = Just (newKnowledge, purity, target)
    unreachable = Nothing
    -- returns True when we are in full control about the stuff that is sensed
    -- e.g. everything on the current cell can only change when out ant explicitly
    -- does some action, but we cannot be certain that some sensed condition will
    -- remain true on other cells.
    sensePersistant Here _ = True
    sensePersistant _    c = c `elem` [Rock, Home, FoeHome]

    removeNonPersitant :: Map Condition a -> Map Condition a
    removeNonPersitant = Map.filterWithKey (\k _ -> sensePersistant Ahead k)


-- | Performs a static analysis of the ant code, automatically deducing
-- knowledge gained by performing certain commands or checks.
-- The number of node traversals can be limited, to account for the limited
-- execution time ants have in the simulator. (Maybe we throw away less information
-- by not finding the actual fixed point.)
--
-- Relies on 'knowledgeBy' for knowledge deduction.
staticAnalysis :: Maybe Int -> CodeGraph -> Graph.Annotated AntState InstructionF Knowledge
staticAnalysis limit g = execState (go 0 startNode initialKnowledge) ag where
  ag = Graph.liftAnnotated g
  startNode = g ^. Graph.initialNode
  -- what we know when the ant program starts
  initialKnowledge = noKnowledge
    & sensesHere .~ Map.fromList (
      [ (Home, True)
      , (FoeHome, False)
      , (Food, False)
      , (FoeMarker, False)
      ] ++
      [ (Marker nr, False) | nr <- [0..5] ])
    & carryingFood .~ Just False

  -- I really hope that this function always terminates....
  -- Actually, it should. The way the static knowledge is combined,
  -- it can only shrink or stay the same. But if it stays the same, we are done.
  -- And it can only be shrank a finite number of times before there is no
  -- knowledge left.
  go :: Int -> AntState -> Knowledge -> State (Graph.Annotated AntState InstructionF Knowledge) ()
  go depth idx newK = do
    -- retrieve current instruction
    instr <- uses (Graph.graph . at idx) fromJust
    -- and current knowledge at that instruction (if already available)
    curK <- use (Graph.annotations . at idx)
    -- combine that knowledge with the knowledge we get as parameter
    let combinedK = maybe newK (combineKnowledge newK) curK
    -- save updated knowledge
    Graph.annotations . at idx .= Just combinedK
    -- check whether we are within the execution limit
    when (maybe True (depth <= ) limit) $
      -- pass knowledge to children if anything changed
      when (Just combinedK /= curK) $ do
        let pass = knowledgeBy instr combinedK
        forOf_ (folded . _Just) pass $ \(k, _, s) -> go (depth + 1) s k

-- | Traverses the code graph starting at the entry point and deletes
-- all unreachable nodes.
deleteUnreachableCode :: CodeGraph -> CodeGraph
deleteUnreachableCode g = foldr Graph.delete g unreachableNodes where
  startNode = view Graph.initialNode g
  allNodes = views Graph.nodes Map.keysSet g

  unreachableNodes = Set.elems $ execState (go startNode) allNodes

  go :: AntState -> State (Set.Set AntState) ()
  go idx = do
    notVisited <- gets (Set.member idx)
    when notVisited $ do
      modify (Set.delete idx)
      iforOf_ (Graph.successors idx) g $ \sucIdx _ -> go sucIdx

-- | Writes the code graph annotated with knowledge to the standard output.
-- Just for debugging purposes.
prettyPrintKnowledge :: Graph.Annotated AntState InstructionF Knowledge -> IO ()
prettyPrintKnowledge g = do
  printf "Entry Point: %d\n" (g ^. Graph.graph . Graph.initialNode)
  iforOf_ (Graph.graph . Graph.nodes . itraversed) g $ \idx val -> do
    print $ g ^. Graph.annotations . at idx
    printf "%5d: %s\n\n" idx (show val)

-- | Uses statically acquired knowledge to simplify the code graph
-- by removing instructions that unnecessary given the current knowledge at
-- that point. For example, if we check that the cell ahead consists of 'Rock',
-- then we are certain that a subsequent 'Move' instruction will fail, as
-- the rock will still be there.
simplify :: Graph.Annotated AntState InstructionF Knowledge -> CodeGraph
simplify g = view Graph.graph result where
  result = execState (go $ g ^. Graph.graph . Graph.initialNode) g

  go :: AntState -> State (Graph.Annotated AntState InstructionF Knowledge) ()
  go idx = do
    -- get current instruction at idx
    g <- get
    instr <- uses (Graph.graph . at idx) (fromMaybe $ error $
      printf "instruction %d not present in program. This should not happen.\n%s" idx (show g))
    -- get knowledge
    curm <- use (Graph.annotations . at idx)
    F.forM_ curm $ \cur -> do
      -- if we have knowledge 'cur', remove it to signify that we already processed
      -- this node
      Graph.annotations . at idx .= Nothing
      -- check if the instruction does anything
      let continuations = toListOf (folded . _Just) $ knowledgeBy instr cur
      case continuations of
        -- no side-effects and a single continuation, we can merge the current
        -- instruction into the next one.
        [(_, Pure, next)] -> Graph.graph %= Graph.mergeNodes next idx
        _ -> return ()
      -- continue with all successors
      forOf_ (folded . _3) continuations go

-- | Runs all available optimizer functions until the number of nodes no longer
-- decreases or the number of iterations reaches the limit passed as first argument.
optimize :: Maybe Int -> CodeGraph -> CodeGraph
optimize (Just n) old
  | n <= 0 = old
optimize limit old
  | Graph.size opt < Graph.size old = optimize (fmap (subtract 1) limit) opt
  | otherwise                       = old -- or "opt", shouldn't matter
  where
    optPass = deleteUnreachableCode . simplify . staticAnalysis Nothing . deleteDuplicates
    opt = optPass old
