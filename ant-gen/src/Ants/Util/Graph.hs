{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE DeriveFoldable        #-}
{-# LANGUAGE DeriveTraversable     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecursiveDo           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeFamilies          #-}
{- | This module provides a graph data structure that is used in the "Ants.Optimizer"
module for traversing the state graph.

It provides lenses and traversals to conviniently access 'nodes' (also using
the 'At' and 'Ix' instances), 'predecessors', 'successors' and the
'inDegree' and 'outDegree' of nodes.

-}
module Ants.Util.Graph
  (
  -- * Graph Data Type
    Graph
  , nodes
  , initialNode

  -- * Accessing Graphs
  , successors
  , predecessors
  , inDegree
  , outDegree
  , size

  -- * Construction Graphs
  , Ants.Util.Graph.empty
  , singleton
  , unfoldGraph

  -- * Modifying Graphs
  , At (..)
  , Ixed (..)
  , mergeNodes
  , insert
  , delete

  -- * Annotated Graphs
  , Annotated (..)
  , liftAnnotated
  , graph
  , annotations
  )
  where

import           Control.Applicative
import           Control.Lens
import           Control.Monad
import           Control.Monad.State
import qualified Data.Foldable       as F
import           Data.Map            (Map)
import qualified Data.Map            as Map
import           Data.Set            (Set)
import qualified Data.Set            as Set
import           Data.Maybe

import qualified Ants.Util.OneToMany as OM

-- | A graph @Graph i f@ is parameterized by the type of node identifiers @i@
-- and a foldable functor @f@ representing the node value and outgoing edges (by
-- applying @f@ to @i@, so the node values are of type @f i@).
--
-- Each item in the functor @f@ thereby represents one outgoing arc.
--
-- Additionally, a graph has an initial node which in case of pogram
-- optimization would be the initial state.
--
-- Lastly, it also maintains a reverse lookup table to find out the parents of
-- a given node.
data Graph i f = Graph
  { _initialNode :: i
  -- ^ see documentation of corresponding lens 'initialNode'
  , _graphNodes       :: Map i (f i)
  -- ^ The map of node identifiers to node values (including outgoing arcs).
  , _reverseArcs :: OM.OneToMany i i
  -- ^ The reverse lookup map, storing all incoming arcs.
  -- __Note__ that this map is maintained by the 'insert' and 'delete' functions,
  -- and thus also by using the 'At' interface of the 'Graph', but obiously not
  -- if the '_graphNodes' field is modified directly.
  }
  deriving (Show, Read, Eq, Ord)

-- | Stores node annotations additional to a graph.
data Annotated i f a = Annotated
  { _graph       :: Graph i f
  , _annotations :: Map i a
  }
  deriving (Eq, Ord, Show, Read, Functor, F.Foldable)

makeLenses ''Graph
makeLenses ''Annotated

-- | A read-only accessor to the map of nodes inside a 'Graph'.
nodes :: Getter (Graph i f) (Map i (f i))
nodes = graphNodes

-- | Lifts a 'Graph' to an 'Annotated' graph by leaving all annotations unset.
liftAnnotated :: Graph i f -> Annotated i f a
liftAnnotated g = Annotated g Map.empty

-- | Helper function that removes all reverse arcs created for the arcs originating at the given position,
-- if there was a node before.
removeArcHelper :: (Ord i, F.Foldable f) => i -> Maybe (f i) -> Graph i f -> Graph i f
removeArcHelper idx = maybe id $ \oldNode -> execState $
  F.for_ oldNode $ \suc ->
    reverseArcs %= OM.delete suc idx

-- | An empty graph with an initial node. This should not be used outside
-- of this module because it returns an incomplete graph.
empty :: i -> Graph i f
empty i = Graph { _initialNode = i, _graphNodes = Map.empty, _reverseArcs = OM.empty}

-- | A graph containing just one node.
singleton :: (Ord i, F.Foldable f) => i -> f i -> Graph i f
singleton i val = insert i val (Ants.Util.Graph.empty i)

-- | Inserts a node into the graph.
insert :: (Ord i, F.Foldable f) => i -> f i -> Graph i f -> Graph i f
insert idx node = execState $ do
  oldNode <- graphNodes . at idx <<.= Just node
  modify $ removeArcHelper idx oldNode
  F.for_ node $ \suc ->
    reverseArcs %= OM.insert suc idx

-- | Removes the node from the graph.
delete :: (Ord i, F.Foldable f) => i -> Graph i f -> Graph i f
delete idx = execState $ do
  removedNode <- graphNodes . at idx <<.= Nothing
  modify $ removeArcHelper idx removedNode

-- | A lens returning the predecessors set of a node.
predecessorsSet :: (Ord i, F.Foldable f) => i -> Getter (Graph i f) (Set i)
predecessorsSet i = reverseArcs . to (OM.lookup i)

-- | A traversal over all successors of a node.
successors :: (Ord i, F.Foldable f) => i -> IndexedTraversal' i (Graph i f) (f i)
successors idx f inputGraph = traverseNodes succs f inputGraph where
  succs = inputGraph ^.. at idx . _Just . folded

-- | A traversal over all predecessors of a node.
predecessors :: (Ord i, F.Foldable f) => i -> IndexedTraversal' i (Graph i f) (f i)
predecessors idx f inputGraph = traverseNodes preds f inputGraph where
  preds = inputGraph ^.. reverseArcs . to (OM.lookup idx) . folded

-- | Traverses a set of node in a graph.
traverseNodes :: At g => [Index g] -> IndexedTraversal' (Index g) g (IxValue g)
traverseNodes [] f g = pure g
traverseNodes (i:is) f g = case g ^. at i of
  Just v -> (\newV newG -> newG & at i .~ Just newV) <$> indexed f i v <*> traverseNodes is f g
  Nothing -> traverseNodes is f g

-- | A getter returning the in-degree of a node.
inDegree :: (Ord i, F.Foldable f) => i -> Getter (Graph i f) Int
inDegree idx = predecessorsSet idx . to Set.size

-- | A getter returning the out-degree of a node.
outDegree :: (Ord i, F.Foldable f) => i -> Getter (Graph i f) Int
outDegree idx = nodes . at idx . to (fromMaybe 0 . fmap (lengthOf folded))

-- | Merges two nodes by only keeping the value of the first,
-- redirecting all arcs connected to the second node to the first node.
mergeNodes :: (Ord i, Functor f, F.Foldable f) => i -> i -> Graph i f -> Graph i f
mergeNodes orig merged = execState $ unless (orig == merged) $ do
  let replace idx = if idx == merged then orig else idx
  -- update all predecessors to point to the new node
  modify $ over (predecessors merged) (fmap replace)
  -- update initial node if that was the one that got merged
  modify $ over initialNode replace
  -- finally remove the merged node
  at merged .= Nothing

-- | A function returning the size of a graph.
size :: Graph i f -> Int
size = views nodes Map.size

type instance Index (Graph i f) = i
type instance IxValue (Graph i f) = f i

instance (Ord i, F.Foldable f) => Ixed (Graph i f) where
  ix = ixAt

instance (Ord i, F.Foldable f) => At (Graph i f) where
  at i = lens getAt setAt where
    getAt g = g ^. nodes . at i
    setAt g Nothing = delete i g
    setAt g (Just v) = insert i v g

-- | Unfolds a graph from an initial seed, a generator function yielding
-- node values for seed values and a function converting seeds to node indices.
unfoldGraph :: (Ord i, Functor f, F.Foldable f) => (s -> f s) -> s -> (s -> i) -> Graph i f
unfoldGraph gen seed idx = execState (go seed) initialGraph where
  initialGraph = Graph { _initialNode = idx seed
                       , _graphNodes  = Map.empty -- Map.singleton seedIdx seedValue
                       , _reverseArcs = OM.empty }
  go s = do
    -- convert seed to index
    let i = idx s
    alreadyThere <- uses (at i) isJust
    unless alreadyThere $ do
      let genVal = gen s
          nodeVal = fmap idx genVal
      at i .= Just nodeVal
      F.for_ genVal go
