{- | This module provides a data type managing a one-to-many relation between
keys and values. It is implemented using 'Map' and 'Set'.
-}
module Ants.Util.OneToMany where

import           Data.Map     (Map)
import qualified Data.Map     as Map
import           Data.Maybe
import           Data.Set     (Set)
import qualified Data.Set     as Set

-- | @OneToMany k v@ is a one-to-many relation between unique values of type @k@
-- and possibly many values of type @v@.
newtype OneToMany k v = OneToMany { rel :: Map k (Set v) } deriving (Show, Read, Eq, Ord)

-- | Checks whether there are no relations.
null :: OneToMany k v -> Bool
null = Map.null . rel

-- | Returns the number of one-to-many relations (counting each source only once).
size :: OneToMany k v -> Int
size = Map.size . rel

-- | Checks whether the given key is the source of a relation.
member :: Ord k => k -> OneToMany k v -> Bool
member k = Map.member k . rel

-- | Checks whether the given key is not the source of a relation.
notMember :: Ord k => k -> OneToMany k v -> Bool
notMember k = Map.notMember k . rel

-- | Retrieves the set of all elements related to the given key.
lookup :: Ord k => k -> OneToMany k v -> Set v
lookup k = fromMaybe Set.empty . Map.lookup k . rel

-- | The empty set of relations.
empty :: OneToMany k v
empty = OneToMany $ Map.empty

-- | Contains exactly one relation from the first to the second argument.
singleton :: k -> v -> OneToMany k v
singleton k v = OneToMany $ Map.singleton k $ Set.singleton v

-- | @insert x y o@ inserts the relation from @x@ to @y@ to @o@.
insert :: (Ord k, Ord v) => k -> v -> OneToMany k v -> OneToMany k v
insert from to = OneToMany . Map.alter go from . rel where
  go Nothing = Just $ Set.singleton to
  go (Just s) = Just $ Set.insert to s

-- | @insert x y o@ removes the relation from @x@ to @y@ to @o@.
-- If the relation does not exist in the first place, the 'OneToMany' is returned
-- unchanged.
delete :: (Ord k, Ord v) => k -> v -> OneToMany k v -> OneToMany k v
delete from to = OneToMany . Map.alter go from . rel where
  go Nothing = Nothing
  go (Just s) =
    let newS = Set.delete to s
    in if Set.null newS then Nothing else Just newS
