{-| Defines a @parsec@ based parser for the high level ant language.
The implementation closely follows the AST defined in "Ants.Parser.AST".
-}
module Ants.Parser.Parser
  (
  -- * Parsers
    parseAntCode
  , parseAntStmt
  , parseAntCond
  -- * Parsec Re-Export
  , parse
  ) where

import Ants.Parser.AST
import Data.Char
import Control.Applicative hiding (many, (<|>))
import Control.Monad.Identity (Identity)
import Text.Parsec
import Text.Parsec.Token
import Text.ParserCombinators.Parsec.Char
import Text.Parsec.Language (haskellDef)
import Text.Parsec.Expr

-- | The parser for top level ant code, consisting of one or more ant statements
parseAntCode :: ParsecT String () Identity AntCode
parseAntCode = whiteSpace lexer *> many parseAntStmt <* eof

-- | Parser for a marker number i.e. one of 0 to 5
parseMarkerNumber :: Parsec String () MarkerNumber
parseMarkerNumber = lexeme lexer (digitToInt <$> oneOf ['0'..'5'])

-- | Defining a lexer with the possible commands and operators
lexer = makeTokenParser haskellDef
  { reservedNames =
      [ "if", "else", "while", "try", "repeat", "label", "goto", "turn left"
      , "turn right", "drop", "mark", "unmark", "here", "ahead"
      , "leftAhead", "rightAhead", "friend", "foe", "friendWithFood"
      , "foeWithFood", "food", "rock", "marker", "foeMarker"
      , "home", "foeHome", "sense", "flip", "pickUp", "move"]
    , reservedOpNames = ["&&", "||", "!", ":"]
  }

-- Below are the parsers for each possible statement.
-- We always use reserved to parse the command.

-- | Covers all possible cases for parsing an ant statement.
parseAntStmt :: Parsec String () AntStmt
parseAntStmt =
  choice [ parseBlock
         , parseIf
         , parseWhile
         , parseRepeat
         , parseTry
         , try parseLabel
         , parseGoTo <* semi lexer
         , parseHaskellApp <* semi lexer
         , parseTurn <* semi lexer
         , parseDrop <* semi lexer
         , parseMark <* semi lexer
         , parseUnmark <* semi lexer
         ]


--All the ant statements parsers
parseBlock :: Parsec String () AntStmt
parseBlock = Block <$> braces lexer (many parseAntStmt)

parseIf :: Parsec String () AntStmt
parseIf = If <$ reserved lexer "if"
             <*> parseAntCond
             <*> parseAntStmt
             <*> optionMaybe (reserved lexer "else" *> parseAntStmt)

parseWhile :: Parsec String () AntStmt
parseWhile = While <$ reserved lexer "while" <*> parseAntCond <*> parseAntStmt

parseRepeat :: Parsec String () AntStmt
parseRepeat = Repeat <$ reserved lexer "repeat"
                     <*> optionMaybe (liftA fromInteger (natural lexer))
                     <*> parseAntStmt

parseTry :: Parsec String () AntStmt
parseTry = Try <$ reserved lexer "try" <*> parseAntCond <* reserved lexer "else" <*> parseAntStmt

parseLabel :: Parsec String () AntStmt
parseLabel = Label <$> identifier lexer
                   <*  reservedOp lexer ":"
                   <*> parseAntStmt

parseGoTo :: Parsec String () AntStmt
parseGoTo = Goto <$ reserved lexer "goto" <*> identifier lexer

parseHaskellApp :: Parsec String () AntStmt
parseHaskellApp = parseApp HaskellApp

parseHaskellCond :: Parsec String () AntCond
parseHaskellCond = parseApp CondHaskell

parseApp :: (AntName -> [AntExpr] -> a) -> Parsec String () a
parseApp f = f <$ char '$' <*> identifier lexer <*> parseArgs

hsString = (:) <$> noneOf ")" <*> manyTill anyChar (lookAhead (string ";;"))

parseArgs :: Parsec String () [AntExpr]
parseArgs = option [] $ parens lexer $ endBy parseArg (string ";;")

parseArg :: Parsec String () AntExpr
parseArg = try $ AntExprStmt <$> parseAntStmt
           <|> AntExprHaskell <$> hsString

parseTurn :: Parsec String () AntStmt
parseTurn = Turn IsLeft <$ reserved lexer "turn left"
                        <|> Turn IsRight
                        <$ string "turn right"

parseDrop :: Parsec String () AntStmt
parseDrop = Drop <$ reserved lexer "drop"

parseMark :: Parsec String () AntStmt
parseMark = Mark <$ reserved lexer "mark" <*> parseMarkerNumber

parseUnmark :: Parsec String () AntStmt
parseUnmark = Unmark <$ reserved lexer "unmark" <*> parseMarkerNumber

-- * Parsing Ant Conditions

-- Below are all the parsers to parse ant conditions

-- | Parser for a direction.
parseSenseDir :: Parsec String () SenseDir
parseSenseDir = Here <$ reserved lexer "here"
                     <|> Ahead <$ reserved lexer "ahead"
                     <|> LeftAhead <$ reserved lexer "leftAhead"
                     <|> RightAhead <$ reserved lexer "rightAhead"

-- | Parser for every possible value of things you can encounter
parseCondition :: Parsec String () Condition
parseCondition = Friend <$ reserved lexer "friend"
                <|> Foe <$ reserved lexer "foe"
                <|> FriendWithFood <$ reserved lexer "friendWithFood"
                <|> FoeWithFood <$ reserved lexer "foeWithFood"
                <|> Food <$ reserved lexer "food"
                <|> Rock <$ reserved lexer "rock"
                <|> Marker <$ reserved lexer "marker" <*> parseMarkerNumber
                <|> FoeMarker <$ reserved lexer "foeMarker"
                <|> Home <$ reserved lexer "home"
                <|> FoeHome <$ reserved lexer "foeHome"

-- | Parsers for proceedings an ant can perform
parseSense :: Parsec String () AntCond
parseSense = CondSense <$ reserved lexer "sense" <*> parseCondition <*> parseSenseDir

parseFlip :: Parsec String () AntCond
parseFlip = CondFlip <$ reserved lexer "flip" <*> liftA fromInteger (natural lexer)

parsePickUp :: Parsec String () AntCond
parsePickUp = CondPickUp <$ reserved lexer "pickUp"

parseMove :: Parsec String () AntCond
parseMove = CondMove <$ reserved lexer "move"


-- | Final function to parse ant conditions that are not operators
parseBasicCond :: Parsec String () AntCond
parseBasicCond = choice [ parseSense
                        , parseFlip
                        , parsePickUp
                        , parseMove
                        , parseHaskellCond ]

-- | Covers all possible cases for parsing an ant conditional expression.
parseAntCond :: Parsec String () AntCond
parseAntCond = buildExpressionParser table term
   <?> "expression"

term :: Parsec String () AntCond
term = parens lexer parseAntCond
   <|> parseBasicCond
   <?> "simple expression"

table :: OperatorTable String () Identity AntCond
table = [ [prefix "!" CondNot ]
        , [binary "&&" CondAnd AssocLeft]
        , [binary "||" CondOr AssocLeft]
        ]

-- Helper functions from the parsec documentation
binary  name fun = Infix (do{ reservedOp lexer name; return fun })
prefix  name fun = Prefix (do{ reservedOp lexer name; return fun })
postfix name fun = Postfix (do{ reservedOp lexer name; return fun })