{-| This module provides the abstract syntax tree (AST) for our high level ant
language.

For a detailed description of the language see <doc/InformalLanguageSpec.md>.
-}
module Ants.Parser.AST
    ( -- * AST
      AntName
    , AntCode
    , AntExpr (..)
    , AntStmt (..)
    , AntCond (..)
    -- * Re-Exports
    ,  AntState
    , LeftOrRight (..)
    , Condition (..)
    , SenseDir (..)
    , MarkerNumber
     ) where

import Simulator (AntState, LeftOrRight (..), Condition (..), SenseDir (..), MarkerNumber)

-- | Name of a label/function.
type AntName = String

-- | The top level ant code is just a list of statements.
type AntCode = [AntStmt]

-- | Represents a single statement. The available statements resemble those
-- that are available in the low level ant language. The syntax/behavior of the
-- control structures are similar to those found in imperative languages like
-- C/Java/etc.
data AntStmt
  = Block [AntStmt]
  -- ^ sequence of commands
  | If AntCond AntStmt (Maybe AntStmt)
  -- ^ If <condition> <thenBranch> <elseBranch>, <elseBranch> is optional
  | While AntCond AntStmt
  -- ^ while loop
  | Repeat (Maybe Int) AntStmt
  -- ^ repeating loop body "n" times or indefinitely
  | Try AntCond AntStmt
  -- ^ runs the statement if the condition evaluates to false
  | Label AntName AntStmt
  -- ^ assigns a label to a statement
  | Goto AntName
  -- ^ unconditional jump to a label
  | HaskellApp AntName [AntExpr]
  -- ^ haskell function application or reference a variable
  | Turn LeftOrRight
  -- ^ turn the ant
  | Drop
  -- ^ Drops food
  | Mark MarkerNumber
  -- ^ Set mark with given number
  | Unmark MarkerNumber
  -- ^ Remove mark with given Number.
  deriving (Show, Read, Eq, Ord)

-- | An expression is an argument to a Haskell function call.
-- It can either be an ant statement (represented as 'AntT' expression) or
-- an almost arbitrary Haskell expression.
data AntExpr = AntExprHaskell String | AntExprStmt AntStmt
  deriving (Show, Read, Eq, Ord)

-- | Represents the boolean conditions we are able to write in the high level
-- language.
data AntCond
  = CondSense Condition SenseDir
  -- ^ translates to a @Sense@ instruction in the low level code.
  | CondFlip Int
  -- ^ uses the built-in random number generator
  | CondPickUp
  -- ^ Tries to pick up food. Evaluates to true if and only if it was successful.
  | CondMove
  -- ^ Tries to move forward. Evaluates to true if and only if it was successful.
  | CondHaskell AntName [AntExpr]
  -- ^ References a Haskell value of type 'BoolExpr'.
  | CondAnd AntCond AntCond
  -- ^ Logical AND
  | CondOr AntCond AntCond
  -- ^ Logical OR
  | CondNot AntCond
  -- ^ Logical NOT
  deriving (Show, Read, Eq, Ord)
