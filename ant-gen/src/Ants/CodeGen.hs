{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecursiveDo           #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TupleSections         #-}
{- | This module provides an embedded domain specific language for generating
low-level ant code.

For this purpose, it exports a number of combinators running in 'AntT' monad.

Internally, running an 'AntT' computation generates the corresponding low-level
ant code.

-}
module Ants.CodeGen
  (
  -- * Code Gen Monad
    AntT
  , Ant
  , runAntT
  , runAnt
  , genAntT
  , genAnt
  -- * Programs
  , Program (..)
  , entryPoint
  , instructions
  -- * Low Level Primitives
  , emit
  , emitSingle
  , emitBranch
  -- * Simple Instructions
  , mark
  , unmark
  , dropFood
  , turn
  -- * Control Flow
  , Label
  , withCont
  , here
  , labelled
  , if'
  , jumpIf
  , continueAt
  , goto
  , failWith
  , thenDo
  , ignore
  -- * Boolean Expressions
  , BoolExpr (..)
  , move
  , pickUp
  , random
  , sense
  -- * Action Primitives
  , move_
  , pickUp_
  , random_
  , sense_
  -- * Loops
  , loop
  , while
  , doWhile
  -- * Re-Exports
  , Instruction (..)
  , AntState
  , LeftOrRight (..)
  , Condition (..)
  , SenseDir (..)
  , MarkerNumber
  , module Control.Monad
  , module Control.Monad.Fix
  ) where

import           Prelude                hiding (not, (&&), (||))
import qualified Prelude

import           Control.Applicative
import           Control.Lens
import           Control.Monad
import           Control.Monad.Fix
import           Control.Monad.State

import           Data.IntMap            (IntMap)
import qualified Data.IntMap            as IntMap

import           Simulator              (AntState, Condition (..),
                                         Instruction (..), LeftOrRight (..),
                                         MarkerNumber, SenseDir (..))

-- | An opaque type representing a program state number.
newtype Label = Label { labelTarget :: AntState }

-- | A low-level ant program consisting of a number of states and corresponding
-- instructions, as well as a starting state.
--
-- __Note__ that due to the way optimizations work, a 'Program' is not required
-- to consist of contigious state numbers or even start at state zero.
-- The numbering is fixed at a later stage ("Ant.Main").
data Program = Program
  { _entryPoint   :: AntState
  -- ^ The initial state of the program where execution begins.
  , _instructions :: IntMap Instruction
  -- ^ A mapping from numbered states to instructions.
  }
  deriving (Eq, Ord, Show, Read)
makeLenses ''Program

-- | This is our code generation monad. It is essentally a two-way state monad,
-- where the program under construction is passed backwards and a supply of
-- state numbers is passed forwards.
--
-- The idea is that for writing a single instruction, we must already know the
-- program that was generated after that instruction to be able to implicitly
-- wire the continuation state to the entry point of the program following
-- that instruction.
--
-- By threading the state number supply forwards, the resulting program has a
-- \"natural\" top down ordering, even through it has been constructed backwards.
newtype AntT m a = AntT { runAntT' :: Program -> AntState -> m (a, Program, AntState) }

-- | Code generation monad without further side effects.
type Ant = AntT Identity

-- | Boolean expressions can be used in conditionals ('if'') and Loops
-- ('while', 'doWhile', 'loop') and allow complex boolean expressions to be
-- translated into ant code.
data BoolExpr
  = BoolAction (forall m. MonadFix m => AntT m () -> AntT m () -> AntT m ())
  -- ^ An arbitrary action that takes a then-branch and an else-branch
  -- and emits the code for the necessary jumps.
  -- This allows the user to built complex predicates on their own, reusing
  -- them in boolean expressions.
  | BoolExpr :&: BoolExpr
  -- ^ Boolean AND
  | BoolExpr :|: BoolExpr
  -- ^ Boolean OR
  | Not BoolExpr
  -- ^ Boolean NOT

infixr 2 :|:
infixr 3 :&:

-- | @runAntT m@ runs the code generation action @f@, returning the result value
-- and the generated program. Any instructions that do not yet have a continuation
-- state are tied back to the first instruction. This means, that the program
-- will automatically loop back to the beginning if it ends.
--
-- __Note__ that this will create a non-terminating computation for the entry point
-- in the case that no instructions have been written.
runAntT :: MonadFix m
        => AntT m a  -- ^ the code generation action
        -> m (a, Program)
runAntT ant = mdo
  (x,prog,_) <- runAntT' ant (Program (view entryPoint prog) IntMap.empty) 0
  return (x, prog)

-- | Like 'runAntT', throwing the result value away.
genAntT :: MonadFix m => AntT m a -> m Program
genAntT = liftM snd . runAntT

-- | 'runAntT' specialized for the identity monad.
runAnt :: Ant a -> (a, Program)
runAnt = runIdentity . runAntT

-- | 'genAntT' specialized for the identity monad.
genAnt :: Ant a -> Program
genAnt = snd . runAnt

instance MonadFix m => Functor (AntT m) where
  fmap = liftM

instance MonadFix m => Applicative (AntT m) where
  pure = return
  (<*>) = ap

instance MonadFix m => Monad (AntT m) where
  return x = AntT $ \prog supply -> return (x, prog, supply)

  mx >>= f = AntT $ \prog supply -> mdo
    (x, prog'', supply') <- runAntT' mx prog' supply
    (y, prog', supply'')  <- runAntT' (f x) prog supply'
    return (y, prog'', supply'')

instance MonadFix m => MonadFix (AntT m) where
  mfix f = AntT $ \prog supply -> mfix $ \ ~(x, _, _) -> runAntT' (f x) prog supply

instance MonadFix m => MonadState Program (AntT m) where
  state f = AntT $ \prog supply -> let (x, prog') = f prog in return (x, prog', supply)

instance MonadTrans AntT where
  lift m = AntT $ \prog supply -> (, prog, supply) `liftM` m

-- | @emit i@ creates a new state with instruction @i@.
-- Note that the instruction has to be fully specified, i.e. the references
-- to continuation states must be set before.
--
-- This function also modifies the entry point of the received program to the
-- written instruction, before the program is passed on to the previous instructions.
-- In that way, the previous instruction automatically knows the \"natural\"
-- continuation state without needing to provide it explicitly.
emit :: MonadFix m => Instruction -> AntT m ()
emit instr = AntT $ \prog supply ->
  let newProg = prog & instructions . at supply .~ Just instr
                     & entryPoint .~ supply
  in return ((), newProg, supply + 1)

-- | @emitSingle f@ passes the current continuation state to @f@ and emits
-- (see 'emit') the instruction returned by @f@.
--
-- It is supposed to be used for single-continuation instructions like
-- ('Drop', 'Mark', etc.)
emitSingle :: MonadFix m => (AntState -> Instruction) -> AntT m ()
emitSingle instr = withCont (emit . instr . labelTarget)

-- | @emitBranch f success failed@ emits the instructions generated by the
-- @success@ and @failed@ actions, passes the starting states of these to @f@
-- and emits the instruction returned as a new entry point.
-- The whole block continues execution with the instructions generated afterwards.
emitBranch :: MonadFix m
           => (AntState -> AntState -> Instruction)
              -- ^ A function receiving the entry point of the success branch
              -- and the failure branch in that order.
           -> AntT m () -- ^ success branch
           -> AntT m () -- ^ failure branch
           -> AntT m ()
emitBranch instr success failed = withCont $ \done -> mdo
  emit $ instr (labelTarget successState) (labelTarget failedState)
  successState <- labelled (success `continueAt` done)
  failedState <- labelled failed
  return ()

-- | @withCont f@ runs a code generation action returned by @f@ with a 'Label'
-- pointing to the continuation state.
withCont :: MonadFix m => (Label -> AntT m a) -> AntT m a
withCont action = AntT $ \prog -> runAntT' (action (prog ^. entryPoint . to Label)) prog

-- | @continueAt m lbl@ runs the action @m@, but the instructions emitted by @m@
-- continue execution at the state pointed by @lbl@ instead of the next instruction
-- after @continueAt@.
continueAt :: MonadFix m => AntT m a -> Label -> AntT m a
continueAt action contState = AntT $ runAntT' action . set entryPoint (labelTarget contState)

-- | Returns a 'Label' to the instruction that is emitted next.
here :: MonadFix m => AntT m Label
here = uses entryPoint Label

-- | @labelled m@ runs @m@ and returns a 'Label' to the entry point of the
-- partial program generated by @m@.
labelled :: MonadFix m => AntT m a -> AntT m Label
labelled action = here <* action

-- | @mark nr@ emits a 'Mark' instruction with mark number @nr@.
mark :: MonadFix m => MarkerNumber -> AntT m ()
mark nr = emitSingle (Mark nr)

-- | @unmark nr@ emits an 'Unmark' instruction with mark number @nr@.
unmark :: MonadFix m => MarkerNumber -> AntT m ()
unmark nr = emitSingle (Unmark nr)

-- | @turn dir@ emits a 'Turn' instruction with direction @dir@.
turn :: MonadFix m => LeftOrRight -> AntT m ()
turn dir = emitSingle (Turn dir)

-- | Emits a 'Drop' instruction.
dropFood :: MonadFix m => AntT m ()
dropFood = emitSingle Drop

-- | @move_ success failed@ emits a 'Move' instruction, jumping to the code
-- generated by @success@ if the move was successful, and jumping to the code
-- generated by @failed@ otherwise. Both branches continue execution at the
-- next instruction after the @move_@ call.
move_ :: MonadFix m => AntT m () -> AntT m () -> AntT m ()
move_ = emitBranch Move

-- | @pickUp_ success failed@ emits a 'PickUp' instruction, jumping to the code
-- generated by @success@ if food was picked up, and jumping to the code
-- generated by @failed@ otherwise. Both branches continue execution at the
-- next instruction after the @pickUp_@ call.
pickUp_ :: MonadFix m => AntT m () -> AntT m () -> AntT m ()
pickUp_ = emitBranch PickUp

-- | @random_ p success failed@ emits a 'Flip' instruction with parameter @p@,
-- jumping to the code generated by @success@ if the random number is zero, and
-- jumping to the code generated by @failed@ otherwise. Both branches continue
-- execution at the next instruction after the @random_@ call.
random_ :: MonadFix m => Int -> AntT m () -> AntT m () -> AntT m ()
random_ p = emitBranch (Flip p)

-- | @sense_ cond dir success failed@ emits a 'Sense' instruction checking for
-- condition @cond@ in direction @dir@, jumping to the code generated by
-- @success@ if the condition is true, and jumping to the code
-- generated by @failed@ otherwise. Both branches continue execution at the
-- next instruction after the @sense_@ call.
sense_ :: MonadFix m => SenseDir -> Condition -> AntT m () -> AntT m () -> AntT m ()
sense_ dir cond = emitBranch (\t f -> Sense dir t f cond)

-- | A 'Move' instruction that can be used in a boolean expression.
-- It evaluates to true if the move was successful, and to false otherwise.
move :: BoolExpr
move = BoolAction move_

-- | A 'PickUp' instruction that can be used in a boolean expression..
-- It evaluates to true if food was picked up, and to false otherwise.
pickUp :: BoolExpr
pickUp = BoolAction pickUp_

-- | A 'Flip' instruction that can be used in a boolean expression.
-- It evaluates to true if the random number is zero, and to false otherwise.
random :: Int -> BoolExpr
random p= BoolAction $ random_ p

-- | A 'Sense' instruction that can be used in a boolean expression.
-- It evaluates the condition on the field given by 'SenseDir'.
sense :: SenseDir -> Condition -> BoolExpr
sense d c = BoolAction $ sense_ d c

-- | @if' cond thenBranch elseBranch@ takes a boolean expression @cond@,
-- and emits the instructions required to jump to the code generated by
-- @thenBranch@ when the condition evaluates to true, and to jump to the
-- code generated by @elseBranch@ otherwise.
if' :: MonadFix m => BoolExpr -> AntT m () -> AntT m () -> AntT m ()
if' expr thenBranch elseBranch = withCont $ \doneLabel -> mdo
    jumpIf expr thenLabel elseLabel
    thenLabel <- labelled $ thenBranch `continueAt` doneLabel
    elseLabel <- labelled elseBranch
    return ()

-- | @jumpIf cond thenLabel elseLabel@ emits the instructions required to jump
-- to the state pointed by @thenLabel@ when @cond@ evaluates to true, and
-- to jump to @elseLabel@ otherwise.
jumpIf :: MonadFix m => BoolExpr -> Label -> Label -> AntT m ()
jumpIf expr thenLabel elseLabel = case expr of
  -- primitive conditions
  BoolAction act -> act (goto thenLabel) (goto elseLabel)
  -- combinators
  e1 :&: e2 -> mdo
    -- if the first check succeeds, we jump to the second check, otherwise
    -- we can exit early.
    jumpIf e1 secondCheck elseLabel
    secondCheck <- labelled $ jumpIf e2 thenLabel elseLabel
    return ()
  e1 :|: e2 -> mdo
    -- if the first check does not succeed, we jump to the second
    start <- jumpIf e1 thenLabel secondCheck
    secondCheck <- labelled $ jumpIf e2 thenLabel elseLabel
    return start
  Not e -> jumpIf e elseLabel thenLabel -- evaluate with swapped labels

-- | @goto lbl@ sets the entry point of the program generated after the use of
-- @goto@ to @lbl@, effectively redirecting jumps to the @goto lbl@ instruction
-- to @lbl@.
goto :: MonadFix m => Label -> AntT m ()
goto lbl = return () `continueAt` lbl

infixr 1 `failWith`
-- | @failWith cond m@ evaluates @cond@. If it was successful, then
-- the evaluation continues at the next instruction, otherwise,
-- it executes the code generated by @m@ before continuing.
-- Similar to 'thenDo'.
failWith :: MonadFix m => BoolExpr -> AntT m () -> AntT m ()
failWith expr handler = withCont $ \doneLabel -> mdo
  startLabel <- jumpIf expr doneLabel handlerLabel
  handlerLabel <- labelled handler
  return startLabel

infixr 1 `thenDo`
-- | @thenDo cond m@ evaluates @cond@. If it failed, then
-- the evaluation continues at the next instruction, otherwise,
-- it executes the code generated by @m@ before continuing.
-- Similar to 'failWith'.
thenDo :: MonadFix m => BoolExpr -> AntT m () -> AntT m ()
thenDo expr success = withCont $ \doneLabel -> mdo
  startLabel <- jumpIf expr successLabel doneLabel
  successLabel <- labelled success
  return startLabel

-- | @ignore exp@ evaluates the condition @exp@ and then continues with the next
-- instruction, regardless of the outcome.
ignore :: MonadFix m => BoolExpr -> AntT m ()
ignore expr = withCont $ \done -> jumpIf expr done done

-- | @loop f@ generates an unconditional loop where the body is provided by
-- @f break continue@, where @break@ is a label that points directly after the
-- loop and @continue@ points to the beginning of the loop.
loop :: MonadFix m => (Label -> Label -> AntT m a) -> AntT m ()
loop body = withCont $ \doneLabel -> mdo
  startLabel <- labelled $ body doneLabel startLabel `continueAt` startLabel
  return ()

-- | @while cond f@ generates an conditional loop where the body is provided by
-- @f break continue@, where @break@ is a label that points directly after the
-- loop and @continue@ points to the beginning of the loop.
-- The condition is evaluated before the body.
while :: MonadFix m => BoolExpr -> (Label -> Label -> AntT m a) -> AntT m ()
while expr body = withCont $ \doneLabel -> mdo
  startLabel <- labelled $ jumpIf expr bodyLabel doneLabel
  bodyLabel <- labelled $ body doneLabel startLabel `continueAt` startLabel
  return ()

-- | @while cond f@ generates an conditional loop where the body is provided by
-- @f break continue@, where @break@ is a label that points directly after the
-- loop and @continue@ points to the beginning of the loop.
-- The condition is evaluated after the body.
doWhile :: MonadFix m => BoolExpr -> (Label -> Label -> AntT m a) -> AntT m ()
doWhile expr body = withCont $ \doneLabel -> mdo
  bodyLabel <- labelled $ body doneLabel condLabel
  condLabel <- labelled $ jumpIf expr bodyLabel doneLabel
  return ()
